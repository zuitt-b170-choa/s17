console.log('drink html');
console.log('eat javascript');
console.log('inhale css');
console.log('bake bootstrap');


/*
	arrays - are used to store multiple ralated data values inside a single variable. Arrays are declared using the square brackets ([]).
	SYNTAX:
		let/const <arrayName> = ["elementA", "elementB", ..., "elementN"]
	
	arrays are used if there is a need to manipulate related values in it.

	index - position of each element in the array. 
*/
let tasks = ["drink", "eat javascript", "inhale css", "bake bootstrap"];
console.log(tasks);
// getting the element using the array's index
console.log(tasks[2]);
/*
	let indexOfLastElement = tasks.length - 1;
	console.log(indexOfLastElement);
*/

// getting the number of elements
console.log(tasks.length);

//trying to access the index of a non-existing element
console.log(tasks[4]);

/*
Array Manipulation

ADDING AN ELEMENT
*/
let numbers = ["one", "two", "three", "four"];
console.log(numbers);
// console.log(numbers[4]);

//Using Assignment Operator
numbers[4] = "five";
console.log(numbers);

//Push Method
numbers.push("element");
console.log(numbers);

//callback function - a function that is passed on/or inserted to another function. this is done because the inserted function is following a particular syntax and the developr is trying to simplify that syntax by just iserting it inside another function.
function pushMethod(element){
	numbers.push(element);
}
pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(numbers);

//REMOVING OF AN ELEMENT
//Pop Method - removing the element at the end of the array
numbers.pop();
console.log(numbers);

function popMethod(){
	numbers.pop();
}

popMethod();
console.log(numbers);

//manipulating the beginning/start of the array
//remove the first element
//Shift Method - adding an element at the start of the array
numbers.shift();
console.log(numbers);

//Callback Function
function shiftMethod(){
	numbers.shift();
}

shiftMethod();
console.log(numbers);

//ADDING AN ELEMENT
//Unshift Method
numbers.unshift("zero");
console.log(numbers);

// Callback Function
function unshiftMethod(element){
	numbers.unshift(element);
}

unshiftMethod("mcdo");
console.log(numbers);

//ARRANGEMENT OF THE ELEMENTS
//sort method - arranges the elements in ascending or descending order. it has an anonymous function inside that has 2 parameters
	// anonymous function - unnamed function and can only be used once
	/*

		SYNTAX:

			arrayName.sort(
			function(a,b){
				return a-b;
			}
			);
		2 parameters inside the anonymous function represents:
			first parameter - first/smallest/starting value
			second parameter - last/biggest/ending element
	*/
//ascending order
let numbs = [15, 27, 32, 12, 6, 8, 236];
console.log(numbs);
numbs.sort(
	function(a,b){
		return a - b;
	}
);

//descending order
console.log(numbs);

numbs.sort(
	function(a,b){
		return b - a;
	}
);

console.log(numbs);

// Reverse Method - reverses the order of the elements in an array. it will depend on the last arrangement of the array, regardless if it is ascending/descending/random order.
numbs.reverse();
console.log(numbs);

//Splice Method
/*
	directly manipulates the array
	first parameter - the index of the element from which the ommitting will begin.
	second parameter - determines the number of elements to be ommitted.
	third parameter - the replacements for the removed elements.
*/
// one paramter: (pure ommission)
// let nums = numbs.splice(1);
// two parameter: (pure ommission)
// let nums = numbs.splice(0,2);
// three parameters: 
let nums = numbs.splice(4,2,31,11,111);
console.log(numbs);
console.log(nums);

// Slice Method - ctrl-c + ctrl-p
/*
	does not affect the original array; creates a sub-array, but does not omit any element from the original array
	first paremeter - index where copying will begin
	second parameter - the number of elements to be copied starting from the first parameter (copying begins at the first parameter)
*/

// one parameter
let slicedNums = numbs.slice(4,6);
console.log(numbs);
console.log(slicedNums);

//Margin of Array
//Concat

console.log(numbers);
console.log(numbs);
let animals = ["dog", "tiger", "kangaroo", "chicken"];
console.log(animals);

let newConcat = numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);

//join method - merges the elements inside the array and makes them a string data
//parameters - separators (space, dash, and nothing at all)
let meal = ["rice", "steak", "juice"];

let newJoin = meal.join();
console.log(newJoin);

newJoin = meal.join("");
console.log(newJoin);

newJoin = meal.join(" ");
console.log(newJoin);

newJoin = meal.join("-");
console.log(newJoin);

//toString Method
//typeof determines the data type of the element after it
console.log(nums);
console.log(typeof nums);
let newString = numbs.toString();
console.log(typeof newString);

//Accessors
let countries = ["US", "PH", "JP", "HK", "SG", "PH", "NZ"];
//indexOf - concerned with the first index it finds from the beginning of the array
/*
	SYNTAX
		arrayName.indexOf();
*/

let index = countries.indexOf("PH");
console.log(index);

//finding a non-existing element - returns -1
index = countries.indexOf("AU");
console.log(index);

//lastIndexOf() - finds the index of the element starting from the end of the array;
/*
	SYNTAX
	arrayName.lastIndexOf();
*/

index = countries.lastIndexOf("PH");
console.log(index);

/*if(countries.indexOf("CAN") === -1){
	console.log("Element not existing.");
}
else{
	console.log("Element exists in the array.")
}*/

//Iterators
//forEach - returns each element in the array
/*
	SYNTAX
		array.forEach(
			function(element){
				statment/s
			}
		)
*/

let days = ["mon", "tues", "wed", "thurs", "fri", "sat", "sun"];
console.log(days);

days.forEach(
	function(element){
		console.log(element);
	});

//map 
/*
	SYNTAX
		array.map(
		function(element){
			statement/s	
		}
	);
*/
// returns a copy of an array from the original which can be manipulated.
let mapDays = days.map(
		function(element){
			return `${element} is the day of the week.`
		}
	)

console.log(mapDays);
console.log(days);

//filter - filters the elements and copies them into another array
console.log(numbs);
let newFilter = numbs.filter(
	function (element){
		return element < 30;
	}
	);

console.log(newFilter);
console.log(numbs);

//includes - return true (boolean) if the element/s is/are inside the aarray

let animalIncludes = animals.includes("dog");
console.log(animalIncludes);

// every - checks if all the elements pass the condition. returns true if all of them passes.
console.log(nums);
let newEvery = nums.every(
		function (element){
			return (element > 10);
		}
	)
console.log(newEvery);

//some
let newSome = nums.some(
	function (element){
		return (element >30);
	})
console.log(newSome);

nums.push(50);
console.log(nums);
//reduce -  performs the operation inside the array to all of the elements.
//first parameter - first element
//second parameter - last element
let newReduce = nums.reduce(
	function(a,b){
		return a+b
	}
	);

console.log(newReduce);


//average
let average = newReduce/nums.length ;
console.log(average);

//toFixed - sets the number of decimal places, to a string.

/*
	parseInt - rounds the number to the nearest whole number.
	parseFloat - rounds the number to the nearest target decimal place(through the use of .toFixed)
*/
console.log(average);
console.log(average.toFixed(2));
console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));
