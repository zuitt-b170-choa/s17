let students = [];

/*
	Array
		students
	Functions
		addStudent() -  pushMethod done
		countStudents() - lengthMethod done
		printStudents() - for()console.log done
		removeStudent - spliceMethod done
		findStudent() - includesMethod done 
		addSection
*/

function addStudent(element){
	students.push(element);
	console.log(`${element} was added to the student's list.`);
}

function countStudents(){
	console.log(`There are a total of ${students.length} student/s enrolled.`);
}

function printStudents(){
	for(let i=0; i<students.length; i++){
		console.log(students[i]);
	}
}


function removeStudent(element){
	let indexRemove = students.indexOf(element);
	let removedStudents = students.splice(indexRemove,1);
	console.log(`${element} was removed from the student's list.`);
}

/*function findStudent(element){
	let ifStudent = students.filter(element);
	if(ifStudent === 1){
		console.log(`${element} is an enrollee`);
	}
	if(ifStudent === 0){
		console.log(`No student found with the name ${element}`);
	}
}*/

function findStudent(element){

	var ifStudent = students.filter(
		function(elements){
			if(elements.length > 1){
			return elements == element;
			};
			if(elements.length === 1){
				let elementIncludes = students.includes(elements)
				return elements == element;
				console.log(elementIncludes);
			};
		}
		);

	if(ifStudent.length === 0){
		console.log(`${ifStudent} is not an enrollee.`);
	}
	if(ifStudent.length === 1){
		console.log(`${ifStudent} is an enrollee.`);
	}
	if(ifStudent.length > 1){
		console.log(`${ifStudent} are enrollees.`);
	}
}

function addSection(element){
let mapStudents = students.map(
	function (elements){
		return `${elements} - Section ${elements}`
	}
	);
console.log(mapStudents);
}